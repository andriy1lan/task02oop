package classes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Scanner;
import classes.Airplane;
import classes.CargoPlane;
import classes.PassengerPlane;

public class AirCompany {

    int c;
    int d;
    List<Airplane> planes;
    PassengerPlane pp;
    CargoPlane cp;

    public void getPassengerPlanes() {
        int counter=0;
        System.out.println("Enter the line of six PassengerPlane object attributes divided by whitespace");
        Scanner sc = new Scanner(System.in);
        while (counter<4) {
            String[] arr = sc.nextLine().split(" ");
            int year=0;
            int maxDistance=0;
            int maxFuel=0;
            double fuelConsuption=0.0;
            int last=0;
            try {
                year=Integer.parseInt(arr[1]);
                maxDistance=Integer.parseInt(arr[2]);
                maxFuel=Integer.parseInt(arr[3]);
                fuelConsuption=Double.parseDouble(arr[4]);
                last=Integer.parseInt(arr[5]);
            }
            catch (Exception ex) {System.out.println("Put the attributes again"); sc.nextLine(); }
            pp=new PassengerPlane(arr[0], year, maxDistance, maxFuel, fuelConsuption, last);
            this.planes.add(pp);
            counter++;
            System.out.println(pp.getName()+" "+ pp.getYear()+" "+ pp.getMaxDistance()+" "+pp.getMaxFuel()+" "+ pp.getFuelConsumption()+" "+pp.getMaxCapacity()); }
    }

    public void getCargoPlanes() {
        int counter=0;
        System.out.println("Enter the line of six CargoPlane object attributes divided by whitespace");
        Scanner sc = new Scanner(System.in);
        while (counter<4) {
            String[] arr = sc.nextLine().split(" ");
            int year=0;
            int maxDistance=0;
            int maxFuel=0;
            double fuelConsuption=0.0;
            int last=0;
            try {
                year=Integer.parseInt(arr[1]);
                maxDistance=Integer.parseInt(arr[2]);
                maxFuel=Integer.parseInt(arr[3]);
                fuelConsuption=Double.parseDouble(arr[4]);
                last=Integer.parseInt(arr[5]);
            }
            catch (Exception ex) {System.out.println("Put the attributes again"); sc.nextLine(); }
            cp=new CargoPlane(arr[0], year, maxDistance, maxFuel, fuelConsuption, last);
            this.planes.add(cp);
            counter++;
            System.out.println(cp.getName()+" "+ cp.getYear()+" "+ cp.getMaxDistance()+" "+cp.getMaxFuel()+" "+ cp.getFuelConsumption()+" "+cp.getMaxCarriage()); }
    }

    public void getInterval() {
        System.out.println("Now find the airplane that complies with some range of fuel consumption");
        Scanner sc = new Scanner(System.in);
        try {
            do { System.out.println("Please enter a positive number as start of interval");
                while (!sc.hasNextInt()) { System.out.println("That is not a positive number");
                    sc.next();}
                c=sc.nextInt();
            } while (c< 0);
            do { System.out.println("Please enter a positive number as end of interval, but grosser then entered before");
                while (!sc.hasNextInt()) { System.out.println("That's not a correct number");
                    sc.next();}
                d=sc.nextInt();
            } while (d <= c);
        }
        catch (Exception ex) {System.out.print(ex.getMessage());}
        System.out.println(this.c+" "+this.d);
    }

    public void printPlanesInfo() {
        for(Airplane ap:this.planes) {
            if (ap instanceof PassengerPlane) {
                System.out.println(ap.getName()+" "+ ap.getYear()+" "+ ap.getMaxDistance()+" "+ap.getMaxFuel()+" "+ ap.getFuelConsumption()+" "+((PassengerPlane)ap).getMaxCapacity()); }
            if (ap instanceof CargoPlane) {
                System.out.println(ap.getName()+" "+ ap.getYear()+" "+ ap.getMaxDistance()+" "+ap.getMaxFuel()+" "+ ap.getFuelConsumption()+" "+((CargoPlane)ap).getMaxCarriage()); }
        }
    }

    public int getGeneralCapacity1() {
        int sum=0;
        sum = planes
                .stream()
                .filter(p -> p instanceof PassengerPlane)
                .mapToInt(o -> ((PassengerPlane)o).getMaxCapacity()).sum();
        return sum;
    }


    public int getGeneralCapacity() {
        int sum=0;
        for(Airplane a:planes) {
            if (a instanceof PassengerPlane) {
                sum+=((PassengerPlane)a).getMaxCapacity();
            }
        }
        return sum;
    }

    public int getGeneralCargoWeight() {
        int sum=0;
        for(Airplane a:planes) {
            if (a instanceof CargoPlane) {
                sum+=((CargoPlane)a).getMaxCarriage();
            }
        }
        return sum;
    }

    public void sortByDistance() {
        planes.sort((Airplane o1, Airplane o2)->o1.getMaxDistance()-o2.getMaxDistance());
    }

    public List<Airplane> findByFuelConsumption(double a, double b) {
        List<Airplane> result = planes.stream()
                .filter(plane->plane.getFuelConsumption()>=a && plane.getFuelConsumption()<=b)
                .collect(Collectors.toList());
        return result;
    }

}