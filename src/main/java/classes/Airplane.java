package classes;

public abstract class Airplane {
    String name;
    int year;
    int maxDistance;
    int maxFuel;
    double fuelConsumption;
//double currentFuel;

    public Airplane (String name, int year, int maxDistance) {
        this.name=name;
        this.year = year;
        this.maxDistance=maxDistance;
    }

    public Airplane () {}

    public Airplane (String name, int year, int maxDistance, int maxFuel, double fuelConsumption) {
        this.name=name;
        this.year = year;
        this.maxDistance = maxDistance;
        this.maxFuel = maxFuel;
        this.fuelConsumption = fuelConsumption;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name=name;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int year) {
        this.year=year;
    }

    public int getMaxDistance() {
        return this.maxDistance;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance=maxDistance;
    }

    public int getMaxFuel() {
        return this.maxFuel;
    }

    public void setMaxFuel(int maxFuel) {
        this.maxFuel=maxFuel;
    }

    public double getFuelConsumption() {
        return this.fuelConsumption;
    }

    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption=fuelConsumption;
    }

    public abstract void fly();


}
