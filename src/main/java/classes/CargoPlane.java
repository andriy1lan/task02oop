package classes;

public class CargoPlane extends Airplane {
    int maxCarriage;
    String sender;
    String client;

    public CargoPlane () {}

    public CargoPlane (String name, int year, int maxDistance, int maxFuel, double fuelConsumption, int maxCarriage) {
        super(name, year, maxDistance, maxFuel, fuelConsumption );
        this.maxCarriage=maxCarriage;
    }

    public int getMaxCarriage() {
        return this.maxCarriage;
    }

    public void setMaxCarriage(int maxCarriage) {
        this.maxCarriage=maxCarriage;
    }

    public String getSender() {
        return this.sender;
    }

    public void setSender(String sender) {
        this.sender=sender;
    }

    public String getClient() {
        return this.client;
    }

    public void setClient (String client) {
        this.client=client;
    }

    public void fly() {
        System.out.println("This plane carry the cargo from:" +this.getSender()+" to: " +this.getClient()+" ");
    }

}