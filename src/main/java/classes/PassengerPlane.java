package classes;

public class PassengerPlane extends Airplane {
    int maxCapacity;
    String departure;
    String destination;

    public PassengerPlane () {}

    public PassengerPlane (String name, int year, int maxDistance, int maxFuel, double fuelConsumption, int maxCapacity) {
        super(name, year, maxDistance, maxFuel, fuelConsumption );
        this.maxCapacity=maxCapacity;
    }

    public int getMaxCapacity() {
        return this.maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity=maxCapacity;
    }

    public String getDeparture() {
        return this.departure;
    }

    public void setDeparture(String departure) {
        this.departure=departure;
    }

    public String getDestination() {
        return this.destination;
    }

    public void setDestination (String destination) {
        this.destination=destination;
    }

    public void fly() {
        System.out.println("This plane fly from:" +this.getDeparture()+" to: " +this.getDestination()+" ");
    }

}