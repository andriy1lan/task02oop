package classes;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import classes.*;
public class Program {

    public static void main(String[] args) {
        AirCompany ac=new AirCompany();
        ac.planes=new ArrayList<Airplane>();
        System.out.println("Enter the info about 4 passenger and 4 cargo airplanes");
        ac.getPassengerPlanes();
        ac.getCargoPlanes();
        ac.printPlanesInfo();

        System.out.println("The general capacity of passenger planes are: "+ac.getGeneralCapacity());
        System.out.println("The general carriage of cargo planes are: "+ac.getGeneralCargoWeight());
        //System.out.println(ac.planes.size());
        System.out.println("Here we could see sorted airplanes accordingly to their maximum distance of flight");
        ac.sortByDistance();
        for(Airplane ap:ac.planes) {System.out.print(ap.getName()+" ");};
        //System.out.println(ac.planes.get(0).getMaxDistance());
        System.out.println();
        ac.getInterval();
        List<Airplane> ll=ac.findByFuelConsumption(ac.c,ac.d);
        if (ll!=null) System.out.println("This plane is complied with fuel range: "+ll.get(0).getName());
    }

}
